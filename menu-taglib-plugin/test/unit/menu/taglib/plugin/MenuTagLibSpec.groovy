package menu.taglib.plugin

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.GroovyPageUnitTestMixin} for usage instructions
 */
@TestFor(MenuTagLib)
class MenuTagLibSpec extends Specification {
    Map render

    def setup() {
        // Inspired from http://stackoverflow.com/a/24428169/267031
        tagLib.metaClass.render = { Map attrs ->
            render = attrs
        }
    }

    void "test the most simple case"() {
        when:
        applyTemplate('<m:menu><m:item/></m:menu>')

        then:
        render.template == '/menu/root'
        render.model.root == [items: [[text: null, url: '#']]]
    }

    void "test a menu item with a link"() {
        when:
        applyTemplate('<m:menu><m:item controller="x" action="y" id="1" params="[a:1,b:2]">Text</m:item></m:menu>')

        then:
        render.template == '/menu/root'
        render.model.root == [items: [[text: 'Text', url: '/x/y/1?a=1&b=2']]]
    }

    void "test a menu item with sub item"() {
        when:
        applyTemplate('<m:menu><m:item text="parent"><m:item controller="x" action="y">Text</m:item></m:item></m:menu>')

        then:
        render.template == '/menu/root'
        render.model.root == [items: [[items: [[text: 'Text', url: '/x/y']],text: 'parent', url: '#']]]
    }
}
