<li class="${item.className} ${item.items ? 'dropdown':''}">
    <g:if test="${item.items}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">${item.text}<span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
        <g:each in="${item.items}" var="subItem">
            <g:render template="/menu/item" model="[item: subItem]" plugin="menu-taglib-plugin"/>
        </g:each>
        </ul>
    </g:if>
    <g:else>
        <a href="${item.url}">${item.text}</a>
    </g:else>
</li>
