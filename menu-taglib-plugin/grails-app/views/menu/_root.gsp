<ul class="nav navbar-nav ${root.className}" id="${root.id}">
    <g:each in="${root.items}" var="item">
        <g:render template="/menu/item" plugin="menu-taglib-plugin" model="[item: item]"/>
    </g:each>
</ul>
