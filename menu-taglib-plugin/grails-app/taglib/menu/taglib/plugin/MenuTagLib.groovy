package menu.taglib.plugin

class MenuTagLib {
    static namespace = "m"
    static defaultEncodeAs = [taglib: 'html']

    def menu = { attrs, body ->
        def root = extractAttributesAndNormalize(attrs, ['id', 'class'])
        // Prepare the pageScope with a list of menu elements
        pageScope.menu = []
        body()
        root.items = pageScope.menu

        out << render(template: '/menu/root', model: [root: root], plugin: 'menu-taglib-plugin' )
    }

    def item = { attrs, body ->
        // Get the current parent level
        def parent = pageScope.menu

        // Get the element attributes
        def menuItem = extractAttributesAndNormalize(attrs, ['elementId', 'class'])

        // Prepare the pageScope.menu for next level
        pageScope.menu = []

        // Extract the body of the item
        def bodyText = body().toString().trim()

        // Did we encounter any sub items?
        if (pageScope.menu) {
            menuItem.items = pageScope.menu
        }

        // Is the item text defined as a message?
        def messageArgs = extractAttributesAndNormalize(attrs, ['code', 'args', 'default'])
        String message = null
        if (messageArgs.default) {
            message = message(messageArgs)
        }

        // Pick the text for the item, starting with bodyText
        menuItem.text = bodyText ?: attrs.remove('text') ?: message

        def href = attrs.remove('href')
        // If there are any more attributes left, try to build a link from them
        def url = null
        if (attrs) {
            url = createLink(attrs)
        }
        menuItem.url = href ?: url ?: '#'

        // Finally add the menu item to the parent list
        parent << menuItem
        // And promote parent to the current pageScope.menu
        pageScope.menu = parent
    }

    /**
     * Helper method. Will take a map of attributes and list of elements to extract from the attributes.
     * It will remove the entries in the attributes that are part of the list and return a new map
     * with those attributes. If the attribute key is 'class' it will be replaced by 'className
     * @param attributes Attributes (will be mutated!)
     * @param toExtract List of attribute keys to extract
     * @return Map of extracted attributes
     */
    private static Map extractAttributesAndNormalize(Map attributes, List toExtract) {
        def extracted = toExtract.collectEntries { [it, attributes.remove(it)] }.findAll { it.value }
        if(extracted.containsKey('class')) {
            extracted.className = extracted.remove('class')
        }
        return extracted
    }
}
