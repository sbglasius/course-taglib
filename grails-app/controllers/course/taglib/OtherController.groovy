package course.taglib

class OtherController {

    def show() {
        render("$controllerName $actionName $params")
    }

    def create() {
        render("$controllerName $actionName $params")
    }
}
