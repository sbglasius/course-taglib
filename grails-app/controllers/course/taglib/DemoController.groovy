package course.taglib

class DemoController {

    def list() {
        render("$controllerName $actionName")
    }

    def show(int index) {
        render("$controllerName $actionName $index")
    }
}
