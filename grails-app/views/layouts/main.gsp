<!DOCTYPE html>
<html>
<head>
    <title><g:layoutTitle default="Grails"/></title>
    <asset:stylesheet src="application.css"/>
    <g:layoutHead/>
</head>
<body>
<div class="container">

    <!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Project name</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <m:menu class="top-menu" id="menu1">
                    <m:item controller="demo" action="list">List</m:item>
                    <m:item controller="demo" action="show" id="1">
                        <g:message code="default.show.label" args="[1]" default="Show"/>
                    </m:item>
                    <m:item text="${message(code: "default.submenu.label", default: 'SubMenu')}">
                        <m:item controller="other" action="show" params="[p1: 'it']"
                                code="default.show.label" args="['item']" default="Show"/>
                        <m:item controller="other" action="create"
                                code="default.create.label" args="['item']" default="Create"/>
                    </m:item>
                </m:menu>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>

    <!-- Main component for a primary marketing message or call to action -->
    <g:layoutBody/>

</div> <!-- /container -->
<asset:javascript src="application.js"/>
</body>
</html>